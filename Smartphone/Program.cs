﻿using Smartphone.Models;
using Smartphone.Utility;
using System.Collections.Generic;
using System;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {
      //this is where your program starts
      //Entry Point

      //instance of smartphone class
      var sc = new SmartphoneClass();

      //set all the properties
      sc.FrontCameraMegapixels = (decimal)1.3;
      sc.Make = "LG";
      sc.Model = "Nexus 5";
      sc.Weight = 512;
      sc.Waterproof = true;
      sc.Bounce = true;
      sc.RearCameraMegapixels = 8;
      sc.ScreenHeight = 1920;
      sc.ScreenSizeIn = (decimal)4.95;
      sc.ScreenWidth = 1080;
      
      //use that new ram class
      sc.Ram = new Ram();
      sc.Ram.Size = (decimal)1210;

      //use that new chassis class
      sc.Chassis = new Chassis();
      sc.Chassis.MaterialTypes = new List<Lookups.MaterialTypes>();
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Aluminum);
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.CarbonFiber);
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Glass);
      sc.Chassis.MaterialTypes.Add(Lookups.MaterialTypes.Plastic);

      //Play with them Apps
      //sc.Apps.name = new List<Lookups.Apps>();

      sc.Apps = new List<App>();
      
      sc.Apps.Add(new App("Drake Shake"));
      sc.Apps.Add(new App("Facebook"));
      sc.Apps.Add(new App("Fuel Cog"));
      sc.Apps.Add(new App("Instagram"));
      sc.Apps.Add(new App("Snapchat"));
      sc.Apps.Add(new App("Spotify"));
      sc.Apps.Add(new App("Tinder"));

      //GPS so Big Brother can keep an eye out.
      sc.RadioFeatures = new RadioFeatures();
      sc.RadioFeatures.HasCdma = true;
      sc.RadioFeatures.HasGps = true;
      sc.RadioFeatures.HasLte = false;
      sc.RadioFeatures.HasWifi = true;

      //use that CPU
      sc.CPU = new CPU();
      sc.CPU.Make = "TSMC";
      sc.CPU.Model = "A8";
      sc.CPU.ClockSpeed = (decimal)2.8;

      //use that OS
      sc.OS = new OS();
      sc.OS.Name = "iOS";
      sc.OS.Version = "7";

      //use that rear camera
      sc.Rear = new Camera();
      sc.Rear.HasFlash = true;
      sc.Rear.ResHeight = 1280;
      sc.Rear.ResWidth = 720;

      //us that front selfie camera
      sc.Front = new Camera();
      sc.Front.HasFlash = false;
      sc.Front.ResHeight = 800;
      sc.Front.ResWidth = 640;

      //use that new screen class
      sc.Screen = new Screen();
      sc.Screen.ScreenType = (Lookups.ScreenTypes.Amoled);
      sc.Screen.ScreenType = (Lookups.ScreenTypes.Lcd);
      sc.Screen.ScreenType = (Lookups.ScreenTypes.Led);
      sc.Screen.IsTouchScreen = true;
      sc.Screen.ScreenSize = 7;
      sc.Screen.ResWidth = 1080;
      sc.Screen.ResHeight = 1920;
      sc.Screen.PPI = 401;

      //Battery life for Drake Shake
      sc.Battery = new Battery();
      sc.Battery.KwH = 1210;
      sc.Battery.IsRemovable = true;
      sc.Battery.MaxTalkTime = (decimal)32.5;

      //write the phone details out to the console
      Console.WriteLine("**** My Smartphone ****");
      Console.WriteLine();
      Console.WriteLine("\r\n Make: {0}, Model: {1}, Weight: {2}, Waterproof: {3}, Bounces: {4}", sc.Make, sc.Model, sc.Weight, sc.Waterproof, sc.Bounce);
      
      Console.WriteLine("\r\n Chasis Specs: Length: {0}, Width: {1}, Thickness: {2}", sc.Chassis.width, sc.Chassis.length, sc.Chassis.thickness);
      foreach (var material in sc.Chassis.MaterialTypes)
      {
        Console.WriteLine("Chasis Material: {0}", material);
      }
      Console.WriteLine("\r\n CPU: Make: {0}, Model: {1}, Clockspeed: {2}", sc.CPU.Make, sc.CPU.Model, sc.CPU.ClockSpeed);
      Console.WriteLine("\r\n RAM: Size: {0}", sc.Ram.Size);
      Console.WriteLine("\r\n Front Camera: {0} MP", sc.FrontCameraMegapixels);
      Console.WriteLine("\r\n Rear Camera: {0} MP", sc.RearCameraMegapixels);
      Console.WriteLine("\r\n OS: Name: {0}, Version: {1}", sc.OS.Name, sc.OS.Version);
      Console.WriteLine("\r\n Radio Stuff: GPS: {0}, Wifi: {1}, LTE: {2}, CDMA: {3}", sc.RadioFeatures.HasGps, sc.RadioFeatures.HasWifi, sc.RadioFeatures.HasLte, sc.RadioFeatures.HasCdma);           
      Console.WriteLine("\r\n Screen Size: {0} in. ({1} mm)", sc.ScreenSizeIn, sc.ScreenSizeMm);
      Console.WriteLine("\r\n Screen Resolution: {0} x {1} pixels", sc.ScreenHeight, sc.ScreenWidth);      
      Console.WriteLine("\r\n Battery: KwH: {0}, Is Removable: {1}, Max Talk Time: {2}", sc.Battery.KwH, sc.Battery.IsRemovable, sc.Battery.MaxTalkTime);
      
      Console.WriteLine("\r\nPre-installed Apps");
      foreach (var install in sc.Apps)
      {
        Console.WriteLine(install.Name);
      }

      Console.WriteLine();
      Console.WriteLine("**** Press the AnyKey ****");
      Console.ReadKey();
    }
  }
}
