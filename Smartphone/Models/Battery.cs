﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartphone.Models
{
  public class Battery
  {
    public int KwH { get; set; }
    public bool IsRemovable { get; set; }
    public decimal MaxTalkTime { get; set; }
  }
}
