﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Chassis
  {
    public List<Lookups.MaterialTypes> MaterialTypes { get; set; }
    public decimal length { get; set; }
    public decimal width { get; set; }
    public decimal thickness { get; set; }
  }
}
