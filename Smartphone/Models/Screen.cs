﻿using Smartphone.Utility;
using System.Collections.Generic;

namespace Smartphone.Models
{
  public class Screen
  {
    public Lookups.ScreenTypes ScreenType { get; set; }
    public bool IsTouchScreen { get; set; }
    public int  ScreenSize { get; set; }
    public int ResWidth { get; set; }
    public int ResHeight { get; set; }
    public int PPI { get; set; }
  }
}
