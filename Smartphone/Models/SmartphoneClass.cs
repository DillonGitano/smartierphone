﻿
using System.Collections.Generic;
namespace Smartphone.Models
{
  /// <summary>
  /// our sweet smartphone class
  /// </summary>
  public class SmartphoneClass
  {
    /// <summary>
    /// Smartphone's RAM Property
    /// </summary>
    public Ram Ram { get; set; }
    
    /// <summary>
    /// Smartphone's Screen Property
    /// </summary>
    public Screen Screen { get; set; }

    /// <summary>
    /// Smartphone's chassis materials
    /// </summary>
    public Chassis Chassis { get; set; }

    /// <summary>
    /// Smartphone's front camera
    /// </summary>
    public Camera Front { get; set; }

    /// <summary>
    /// Smartphone's rear camera
    /// </summary>
    public Camera Rear { get; set; }

    /// <summary>
    /// Smartphone's Radio stuff
    /// </summary>
    public RadioFeatures RadioFeatures { get; set; }

    /// <summary>
    /// Juice me up
    /// </summary>
    public Battery Battery { get; set; }

    /// <summary>
    /// The entire point of owning a smartphone
    /// </summary>
    public List<App> Apps { get; set; }

    /// <summary>
    /// Smartphone's OS
    /// </summary>
    public OS OS { get; set; }

    /// <summary>
    /// Smartphone's CPU
    /// </summary>
    public CPU CPU { get; set; }
    
    /// <summary>
    /// The make of the smartphone.
    /// </summary>
    public string Make { get; set; }

    public int Weight { get; set; }

    public bool Waterproof { get; set; }

    public bool Bounce { get; set; }

    /// <summary>
    /// The model of the smartphone.
    /// </summary>
    public string Model { get; set; }

    /// <summary>
    /// Phone's diagonal screen size in inches.
    /// </summary>
    public decimal ScreenSizeIn { get; set; }

    /// <summary>
    /// Read-only screen size in mm.
    /// </summary>
    public decimal ScreenSizeMm
    {
      get
      {
        return ScreenSizeIn * (decimal)25.4;
      }
    }
       
    /// <summary>
    /// Screen width in pixels.
    /// </summary>
    public int ScreenWidth { get; set; }

    /// <summary>
    /// Screen height in pixels.
    /// </summary>
    public int ScreenHeight { get; set; }


    //public Camera FrontCamera { get; set; }
    /// <summary>
    /// Front camera megapixels.
    /// </summary>
    public decimal FrontCameraMegapixels { get; set; }

    //public Camera RearCamera { get; set; }
    /// <summary>
    /// Rear camera megapixels.
    /// </summary>
    public decimal RearCameraMegapixels { get; set; }
  }
}
