﻿
namespace Smartphone.Models
{
  public class Ram
  {
    /// <summary>
    /// RAM size in MegaBytes
    /// </summary>
    public decimal Size { get; set; }
  }
}
