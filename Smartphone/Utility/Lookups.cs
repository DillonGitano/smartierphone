﻿
namespace Smartphone.Utility
{
  public static class Lookups
  {
    public enum ScreenTypes
    {
      Amoled = 1,
      Lcd = 2,
      Led = 3
    }

    public enum MaterialTypes
    {
      Aluminum = 1,
      CarbonFiber = 2,
      Glass = 3,
      Plastic = 4
    }
  }
}
